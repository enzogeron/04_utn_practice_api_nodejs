const express = require('express');
const exec = require('../config/database');

const router = express.Router();

/* RUTAS PARA PERSONA */

/*
    -- Se crea una persona en la base de datos, tabla personas
    POST: /persona
    STATUS: 200
        body: {
            nombre: string,
            apellido: string,
            alias: string,
            email: string
        }
        response: {
            id: numerico,
            nombre: string,
            apellido: string,
            alias: string,
            email: string
        }
    STATUS: 413
        response: {
            mensaje: "faltan datos" || "el email ya existe" || "error inesperado"
        }
*/
router.post('/', async (req, res) => {
  try {
    // Desestructuracion de objetos
    const { nombre, apellido, email, alias } = req.body;

    // Comprobar que todos los campos tengan informacion
    if (
      !nombre ||
      !apellido ||
      !email ||
      !alias ||
      nombre.trim() === '' ||
      apellido.trim() === '' ||
      email.trim() === '' ||
      alias.trim() === ''
    ) {
      return res.status(413).json({
        mensaje: 'Todos los datos de la persona son obligatorios.',
      });
    }

    // Comprobar que el email no exista en la base de datos
    let query = 'SELECT email FROM personas WHERE LOWER(email) = ?';
    const checkEmail = await exec(query, email.toLowerCase());
    if (checkEmail.length) {
      return res.status(413).json({
        mensaje: 'El email ya se encuentra registrado.',
      });
    }

    // Creamos la persona en la base de datos
    query =
      'INSERT INTO personas(nombre, apellido, email, alias) VALUES(?, ?, ?, ?)';
    const data = await exec(query, [nombre, apellido, email, alias]);

    res.json({
      id: data.insertId,
      nombre,
      apellido,
      email,
      alias,
    });
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se consulta la lista de personas
    GET: /persona
    STATUS: 200
        response: [
          { id: numerico, nombre: string, apellido: string, alias: string, email: string }
          { id: numerico, nombre: string, apellido: string, alias: string, email: string }
          ...
          { id: numerico, nombre: string, apellido: string, alias: string, email: string }
        ]
    STATUS: 413
        response: []
*/
router.get('/', async (req, res) => {
  try {
    const query = 'SELECT id, nombre, apellido, alias, email FROM personas';
    const data = await exec(query);

    if (!data.length) {
      return res.status(413).json([]);
    }

    res.json(data);
  } catch (e) {
    res.status(500).json('Error inesperado.');
  }
});

/*
    -- Se consulta la informacion de una persona
    GET: /persona/:id
    STATUS: 200
        response: {
          id: numerico,
          nombre: string,
          apellido: string,
          alias: string,
          email: string
        }
    STATUS: 413
        response: {
          mensaje: "error inesperado" || "no se encuentra esa persona"
        }
*/
router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const query = 'SELECT * FROM personas WHERE id = ?';
    const checkPerson = await exec(query, id);

    if (!checkPerson.length) {
      return res.status(413).json({
        mensaje: 'No se encuentra esa persona en la base de datos.',
      });
    }

    res.json(checkPerson[0]);
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se actualiza la informacion de una persona, no se puede modificar el email
    PUT: /persona/:id
    STATUS: 200
        body: {
          nombre: string,
          apellido: string,
          alias: string,
          email: string
        }
        response: {
          id: numerico,
          nombre: string,
          apellido: string,
          alias: string,
          email: string
        }
    STATUS: 413
        response: {
          mensaje: "error inesperado" || "no se encuentra esa persona"
        }
*/
router.put('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    let { nombre, apellido, alias, email } = req.body;

    // Comprobar que todos los campos tengan informacion
    if (
      !nombre ||
      !apellido ||
      !email ||
      !alias ||
      nombre.trim() === '' ||
      apellido.trim() === '' ||
      email.trim() === '' ||
      alias.trim() === ''
    ) {
      return res.status(413).json({
        mensaje: 'Todos los datos de la persona son obligatorios.',
      });
    }

    // Comprobar que la persona exista en la base de datos
    let query = 'SELECT email FROM personas WHERE id = ?';
    const checkPerson = await exec(query, id);

    if (!checkPerson.length) {
      return res.status(413).json({
        mensaje: `No existe una persona en la base de datos, con el id: ${id}`,
      });
    }

    // Comprobar que no se modifico el email (uso la query anterior)
    if (email !== checkPerson[0].email) {
      return res.status(413).json({
        mensaje: 'No puede modificar el email.',
      });
    }

    nombre = nombre.trim();
    apellido = apellido.trim();
    alias = alias.trim();
    query =
      'UPDATE personas SET nombre = ?, apellido = ?, alias = ? WHERE id = ?';
    const updatePerson = await exec(query, [nombre, apellido, alias, id]);

    res.json({
      id,
      nombre,
      apellido,
      alias,
      email,
    });
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se elimina una persona
    DELETE: /persona/:id
    STATUS: 200
        response: {
          mensaje: "se elimino correctamente"
        }
    STATUS: 413
        response: {
          mensaje: "error inesperado" || "no se encuentra esa persona" || "la persona tiene
          libros asociados, no se puede eliminar"
        }
*/
router.delete('/:id', async (req, res) => {
  try {
    const id = req.params.id;

    // Comprobar que la persona exista en la base de datos
    let query = 'SELECT email FROM personas WHERE id = ?';
    const checkPerson = await exec(query, id);

    if (!checkPerson.length) {
      return res.status(413).json({
        mensaje: `No existe una persona en la base de datos, con el id: ${id}`,
      });
    }

    // Comprobar que la persona no tenga libros asociados
    query = 'SELECT id FROM libros WHERE persona_id = ?';
    const checkBooks = await exec(query, id);

    if (checkBooks.length) {
      return res.status(413).json({
        mensaje: 'La persona tiene libros asociados, no se puede borrar.',
      });
    }

    query = 'DELETE FROM personas WHERE id = ?';
    const deletePerson = await exec(query, id);

    res.json({
      mensaje: 'Se elimino de la base de datos correctamente.',
    });
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

module.exports = router;
