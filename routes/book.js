const express = require('express');
const { check } = require('prettier');
const exec = require('../config/database');

const router = express.Router();

/* RUTAS PARA LIBRO */

/*
    -- Se crea un libro en la base de datos, tabla libros
    POST: /libro
    STATUS: 200
        body: {
            nombre: string,
            descripcion: string,
            categoria_id: numero,
            persona_id: numero|null
        }
        response: {
            id: numerico,
            nombre: string,
            descripcion: string,
            categoria_id: numero,
            persona_id: numero|null
        }
    STATUS: 413
        response: {
            mensaje: "faltan datos" || "no existe la categoria"
            || "no existe la persona" || "error inesperado" || "el libro ya existe"
        }
*/
router.post('/', async (req, res) => {
  try {
    let { nombre, descripcion, categoria_id } = req.body;
    let persona_id = req.body.persona_id || null;

    // Comprobar que los datos no esten vacios
    if (!nombre || !categoria_id || nombre.trim() === '') {
      return res.status(413).json({
        mensaje: 'El nombre y la categoria son datos obligatorios.',
      });
    }

    // Comprobar que el libro no existe
    let query = 'SELECT nombre FROM libros WHERE LOWER(nombre) = ?';
    const checkBook = await exec(query, nombre.toLowerCase());
    if (checkBook.length) {
      return res.status(413).json({
        mensaje: 'Existe un libro registrado con el mismo nombre.',
      });
    }

    // Comprobar que la categoria_id existe en la tabla CATEGORIAS
    query = 'SELECT id FROM categorias WHERE id = ?';
    const checkCategory = await exec(query, categoria_id);
    if (!checkCategory.length) {
      return res.status(413).json({
        mensaje: 'No existe la categoria.',
      });
    }

    nombre = nombre.trim();
    descripcion = descripcion.trim();
    let values = [nombre, descripcion, categoria_id, persona_id];

    // Condicion para determinar si se envio persona_id
    if (persona_id && persona_id.trim() !== '') {
      // Comprobar que la persona_id existe en la tabla PERSONAS
      query = 'SELECT id FROM personas WHERE id = ?';
      const checkPerson = await exec(query, persona_id);

      if (!checkPerson.length) {
        return res.status(413).json({
          mensaje: 'No existe la persona.',
        });
      }

      query =
        'INSERT INTO libros(nombre, descripcion, categoria_id, persona_id) VALUES(?, ?, ?, ?)';

      persona_id = Number(persona_id);
    } else {
      values = values.slice(0, -1);
      query =
        'INSERT INTO libros(nombre, descripcion, categoria_id) VALUES(?, ?, ?)';
    }

    const data = await exec(query, values);

    categoria_id = Number(categoria_id);

    res.json({
      id: data.insertId,
      nombre,
      descripcion,
      categoria_id,
      persona_id,
    });
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se consulta la lista de libros
    GET: /libro
    STATUS: 200
        response: [
          { id: numerico, nombre: string, descripcion: string...... }
          { id: numerico, nombre: string, descripcion: string...... }
          ...
          { id: numerico, nombre: string, descripcion: string...... }
        ]
    STATUS: 413
        response: {
            mensaje: "error inesperado"
        }
*/
router.get('/', async (req, res) => {
  try {
    const query = 'SELECT * FROM libros';
    const data = await exec(query);

    if (!data.length) {
      return res.status(413).json([]);
    }

    res.json(data);
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se consulta la informacion de un libro
    GET: /libro/:id
    STATUS: 200
        response: {
          id: numerico,
          nombre: string,
          descripcion: string,
          categoria_id: numero,
          persona_id: numero|null
        }
    STATUS: 413
        response: {
            mensaje: "error inesperado" || "no se encuentra ese libro"
        }
*/
router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const query = 'SELECT * FROM libros WHERE id = ?';
    const checkBook = await exec(query, id);

    if (!checkBook.length) {
      return res.status(413).json({
        mensaje: 'El libro no existe en la base de datos',
      });
    }

    res.json(checkBook[0]);
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se modifica la informacion de un libro
    PUT: /libro/:id
    STATUS: 200
        body {
          id: numerico,
          nombre: string,
          descripcion: string,
          categoria_id: numero,
          persona_id: numero|null
        }
        response: {
          id: numerico,
          nombre: string,
          descripcion: string,
          categoria_id: numero,
          persona_id: numero|null
        }
    STATUS: 413
        response: {
            mensaje: "error inesperado" || "solo se puede modificar la descripcion del libro"
        }
*/
router.put('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const descripcion = req.body.descripcion;

    // Se comprueba que la descripcion no sea null y no este vacia
    if (!descripcion || descripcion.trim() === '') {
      return res.status(500).json({
        mensaje: 'El campo descripcion es obligatorio',
      });
    }

    // Se comprueba que el id del libro sea valido
    let query = 'SELECT * FROM libros WHERE id = ?';
    const checkBook = await exec(query, id);
    if (!checkBook.length) {
      return res.status(413).json({
        mensaje: 'El id del libro no existe en la base de datos.',
      });
    }

    // Actualizar descripcion del libro
    query = 'UPDATE libros SET descripcion = ? WHERE id = ?';
    const updateBook = await exec(query, [descripcion, id]);

    // Se retornar checkBook porque en la primer query = 'SELECT ...' se obtiene la info
    // que necesita el response, checkBook es un array por eso va con [0]
    // solo se sobreescribe el valor de descripcion, en la primer query esta desactualizado
    checkBook[0].descripcion = descripcion.trim();
    res.json(checkBook[0]);
  } catch (e) {
    return res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se realiza el prestamo de un libro
    PUT: /libro/prestar/:id
    STATUS: 200
        body {
          id: numerico,
          persona_id: numerico|null
        }
        response: {
          mensaje: "se presto correctamente el libro"
        }
    STATUS: 413
        response: {
            mensaje: "error inesperado" || "el libro ya se encuentra prestado..."
            || "no se encontro el libro" || "no se encontro a la persona..."
        }
*/
router.put('/prestar/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const persona_id = req.body.persona_id || null;
    let query = '';

    // Comprobar si el libro existe
    query = 'SELECT persona_id FROM libros WHERE id = ?';
    const checkBook = await exec(query, id);

    if (!checkBook.length) {
      return res.status(413).json({
        mensaje: 'No existe el libro en la base de datos.',
      });
    }

    // Comprobar si la persona es null o existe
    if (persona_id && persona_id.trim() !== '') {
      query = 'SELECT * FROM personas WHERE id = ?';
      const checkPerson = await exec(query, persona_id);
      if (!checkPerson.length) {
        return res.status(413).json({
          mensaje: 'No existe la persona en la base de datos.',
        });
      }
    } else {
      return res.status(413).json({
        mensaje:
          'No puede realizar un prestamo porque persona_id no es valido.',
      });
    }

    // Utilizar la query anterior para saber si el libro ya se encuentra prestado
    if (checkBook[0].persona_id !== null) {
      return res.status(413).json({
        mensaje:
          'El libro ya se encuentra perstado, no se puede prestar hasta que no se devuelva',
      });
    }

    query = 'UPDATE libros SET persona_id = ? WHERE id = ?';
    const borrowBook = await exec(query, [persona_id, id]);

    res.json({ message: 'Se presto correctamente el libro.' });
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se devuelve un libro que estaba prestado
    PUT: /libro/devolver/:id
    STATUS: 200
        response: {
          mensaje: "se realizo la devolucion correctamente"
        }
    STATUS: 413
        response: {
            mensaje: "error inesperado" || "ese libro no estaba prestado"
            || "no se encontro el libro"
        }
*/
router.put('/devolver/:id', async (req, res) => {
  try {
    const id = req.params.id;
    // Comprobar si el libro existe en la base de datos
    let query = 'SELECT persona_id FROM libros WHERE id = ?';
    const checkBook = await exec(query, id);

    if (!checkBook.length) {
      return res.status(413).json({
        mensaje: 'El libro no existe en la base de datos.',
      });
    }

    // Utilizo la query anterior para saber si el libro no estaba prestado
    if (checkBook[0].persona_id === null) {
      return res.status(413).json({
        mensaje: 'El libro no estaba prestado.',
      });
    }

    query = 'UPDATE libros SET persona_id = NULL WHERE id = ?';
    const updateBook = await exec(query, id);
    res.json({
      mensaje: 'Se realizo la devolucion correctamente.',
    });
  } catch (e) {
    return res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se elimina un libro de la base de datos
    DELETE: /libro/:id
    STATUS: 200
        response: {
          mensaje: "se elimino el libro correctamente"
        }
    STATUS: 413
        response: {
            mensaje: "error inesperado" || "el libro esta prestado no se puede eliminar"
            || "no se encontro el libro"
        }
*/
router.delete('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    // Comprobar que el libro exista en la base de datos
    let query = 'SELECT persona_id FROM libros WHERE id = ?';
    const checkBook = await exec(query, id);

    if (!checkBook.length) {
      return res.status(413).json({
        mensaje: 'El libro no existe en la base de datos.',
      });
    }

    // Utilizo la query anterior para saber si persona_id es null, si no lo es
    // significa que el libro esta prestado y no se puede borrar
    if (checkBook[0].persona_id !== null) {
      return res.status(413).json({
        mensaje: 'El libro esta prestado no es posible borrarlo.',
      });
    }

    query = 'DELETE FROM libros WHERE id = ?';
    const deleteBook = await exec(query, id);

    res.json({
      mensaje: 'Se elimino el libro correctamente.',
    });
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

module.exports = router;
