const express = require('express');
const exec = require('../config/database');

const router = express.Router();

/* RUTAS PARA CATEGORIA */

/*
    -- Se crea una categoria en la base de datos, tabla categorias
    POST: /categoria
    STATUS: 200
        body: {
            nombre: string
        }
        response: { id: numerico, nombre: string}
    STATUS: 413
        response: {
            mensaje: "faltan datos" || "ese nombre de categoría ya existe" || "error inesperado"
        }
*/
router.post('/', async (req, res) => {
  try {
    // Se recibe el nombre de la categoria
    let name = req.body.nombre;

    // Comprobamos que el nombre no este vacio
    if (!name || name.trim() === '') {
      return res.status(413).json({
        mensaje: 'El nombre de la categoria es obligatorio.',
      });
    }

    // Comprobamos que el nombre no sea repetido
    let query = 'SELECT nombre FROM categorias WHERE LOWER(nombre) = ?';
    const checkName = await exec(query, name.toLowerCase());

    if (checkName.length) {
      return res.status(413).json({
        mensaje: 'El nombre ingresado para la categoria ya existe.',
      });
    }

    // Creamos la categoria en la base de datos
    name = name.trim();
    query = 'INSERT INTO categorias(nombre) VALUES(?)';
    const data = await exec(query, name);
    res.json({
      id: data.insertId,
      nombre: name,
    });
  } catch (e) {
    return res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se consulta la lista de categorias
    GET: /categoria
    STATUS: 200
      response: [
          {id: numerico, nombre: string},
          {id: numerico, nombre: string},
          .....
          {id: numerico, nombre: string}
      ]
    STATUS: 413
      response: []
*/
router.get('/', async (req, res) => {
  try {
    const query = 'SELECT id, nombre FROM categorias';
    const data = await exec(query);
    // Si la consulta query no tiene resultados se retorna status: 413 y []
    if (!data.length) {
      return res.status(413).json(data);
    }
    res.json(data);
  } catch (e) {
    res.status(500).json('Error inesperado.');
  }
});

/*
    -- Se consulta la informacion de una categoria por id, es un parametro de la url
    GET: /categoria/:id
    STATUS: 200
      response: {
        id: numerico,
        nombre: string
      }
    STATUS: 413
      response: {
        mensaje: "error inesperado" || "categoria no encontrada"
      }
*/
router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id;

    // Compruebo que la categoria no exista
    const query = 'SELECT id, nombre FROM categorias WHERE id = ?';
    const checkCategory = await exec(query, id);

    if (!checkCategory.length) {
      return res.status(413).json({
        mensaje: 'La categoria no existe.',
      });
    }

    // Si llego aqui, la categoria existe y devuelvo su informacion
    return res.json(checkCategory[0]);
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

/*
    -- Se elimina una categoria
    DELETE: /categoria/:id
    STATUS: 200
      response: {
        mensaje: "Se borro correctamente."
      }
    STATUS: 413
      response: {
        mensaje: "error inesperado" || "categoria con librtos asociados, no se puede eliminar"
        || "no existe la categoria indicada"
      }
*/
router.delete('/:id', async (req, res) => {
  try {
    const id = req.params.id;

    // Compruebo que la categoria no exista
    let query = 'SELECT id, nombre FROM categorias WHERE id = ?';
    const checkCategory = await exec(query, id);

    if (!checkCategory.length) {
      return res.status(413).json({
        mensaje: 'La categoria no existe.',
      });
    }

    // Compruebo si tiene libros asociados
    query = 'SELECT categoria_id FROM libros WHERE categoria_id = ?';
    const checkBooks = await exec(query, id);

    if (checkBooks.length) {
      return res.status(413).json({
        mensaje: 'La categoria tiene libros asociados, no se puede borrar.',
      });
    }

    query = 'DELETE FROM categorias WHERE id = ?';
    const deleteCategory = await exec(query, id);

    res.json({
      mensaje: 'La categoria se borro correctamente.',
    });
  } catch (e) {
    res.status(500).json({
      mensaje: 'Error inesperado.',
    });
  }
});

module.exports = router;
